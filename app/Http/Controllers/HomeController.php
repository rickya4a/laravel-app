<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $content['product'] = Product::all();
        $content['latest_product'] = Product::where('status', false)
                                            ->orderBy('created_at')
                                            ->take(10)
                                            ->get();

        return view('welcome', $content);
    }
}
