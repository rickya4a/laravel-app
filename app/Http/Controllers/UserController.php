<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $content['products'] = Product::where('status', true)
                                        ->orderBy('created_at')
                                        ->get();
        $content['product_count'] = Product::count();
        $content['user_count'] = User::count();
        $content['product_count_active'] = Product::where('status', true)->count();
        $content['user_count_active'] = User::where('is_active', true)->count();

        return view('admin.dashboard', $content);
    }

    public function users()
    {
        $content['data'] = User::where('is_admin', false)
                                ->orderBy('created_at')
                                ->get();

        return view('admin.user', $content);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        $user = Auth::user();

        return view('auth.login');
    }

    public function auth(Request $request)
    {
        $validated = $request->validate([
            'email_or_phone' => 'required',
            'password' => 'required'
        ]);

        $get_user = User::where('email', $request->input('email_or_phone'))
                                 ->orWhere('phone', $request->input('email_or_phone'))
                                 ->first();

        if ($get_user) {

            if (Hash::check($request->input('password'), $get_user->password)) {
                if (Auth::attempt(['email' => $get_user->email, 'password' => $request->password])) {
                    $request->session()->regenerate();

                    if (Auth::user()->is_admin) {
                        return redirect()->intended('/admin/dashboard');
                    } else {
                        return redirect()->intended('/');
                    }
                }
            }
        }


        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ])->onlyInput('email');

    }

    public function register()
    {
        return view('auth.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'phone' => 'required|numeric',
            'email' => 'required|email'
        ]);

        $user = User::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'password' => Hash::make('password')
        ]);

        return redirect('/admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
