@extends('components.layout')

@include('components.navbar')

@include('components.sidebar')

@section('content')
    <div class="container">
        <div class="grid grid-cols-1 h-40 sm:grid-cols-2 lg:grid-cols-4 p-4 gap-4">
            <div
                class="bg-blue-300 dark:bg-gray-800 shadow-lg rounded-md flex items-center justify-between p-3 dark:border-gray-600">
                <div class="flex flex-col">
                    <p class="text-gray-500 text-md font-light">Jumlah User</p>
                    <p class="text-blue-950 text-2xl">{{ $user_count }} User</p>
                </div>
            </div>
            <div
                class="bg-blue-300 dark:bg-gray-800 shadow-lg rounded-md flex items-center justify-between p-3 dark:border-gray-600">
                <div class="flex flex-col">
                    <p class="text-gray-500 text-md font-light">Jumlah User Aktif</p>
                    <p class="text-blue-950 text-2xl">{{ $user_count_active }} User</p>
                </div>
            </div>
            <div
                class="bg-blue-300 dark:bg-gray-800 shadow-lg rounded-md flex items-center justify-between p-3 dark:border-gray-600">
                <div class="flex flex-col">
                    <p class="text-gray-500 text-md font-light">Jumlah Produk</p>
                    <p class="text-blue-950 text-2xl">{{ $product_count }} Produk</p>
                </div>
            </div>
            <div
                class="bg-blue-300 dark:bg-gray-800 shadow-lg rounded-md flex items-center justify-between p-3 dark:border-gray-600">
                <div class="flex flex-col">
                    <p class="text-gray-500 text-md font-light">Jumlah Produk Aktif</p>
                    <p class="text-blue-950 text-2xl">{{ $product_count_active }} Produk</p>
                </div>
            </div>
        </div>

        <div class="grid grid-cols-1 lg:grid-cols-2 p-4 gap-4">
            <div
                class="relative flex flex-col min-w-0 mb-4 lg:mb-0 break-words bg-gray-50 dark:bg-gray-800 w-full shadow-lg rounded">
                <div class="rounded-t mb-0 px-0 border-0">
                    <div class="flex flex-wrap items-center px-4 py-2">
                        <div class="relative w-full max-w-full flex-grow flex-1">
                            <h3 class="font-semibold text-base text-gray-900 dark:text-gray-50">Produk Terbaru</h3>
                        </div>
                    </div>
                    <div class="block w-full overflow-x-auto">
                        <table class="items-center w-full bg-transparent border-collapse">
                            <thead class="bg-blue-500 rounded-md">
                                <tr>
                                    <th
                                        class="px-4 dark:bg-gray-600 text-white dark:text-gray-100 align-middle border border-solid border-gray-200 dark:border-gray-500 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                                        Produk</th>
                                    <th
                                        class="px-4 dark:bg-gray-600 text-white dark:text-gray-100 align-middle border border-solid border-gray-200 dark:border-gray-500 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                                        Tanggal Dibuat</th>
                                    <th
                                        class="px-4 dark:bg-gray-600 text-white dark:text-gray-100 align-middle border border-solid border-gray-200 dark:border-gray-500 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                                        Harga(Rp)</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $row)
                                    <tr class="text-gray-700 dark:text-gray-100">
                                        <th
                                            class="border-t-0 px-4 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
                                            {{ $row->name }}</th>
                                        <td
                                            class="border-t-0 px-4 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                            {{ $row->created_at }}</td>
                                        <td
                                            class="border-t-0 px-4 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                            {{ $row->price }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
