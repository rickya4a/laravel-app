@extends('components.layout')

@section('content')
    <div class="flex">
        <div class="w-full bg-blue-400 text-center flex justify-center items-center">
            <span class="mx-auto text-lg text-black">
                <h1 class="text-2xl capitalize font-semibold">Laravel App</h1>
                <p class="w-80">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                    been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of
                    type and scrambled it to make a type specimen book.</p>
            </span>
        </div>
        <div class="w-full m-3 min-h-full">
            @if ($errors->any())
                <ul>
                    @foreach ($errors->all() as $error)
                        <li class="my-2">
                            <div x-data
                                class="text-sm text-left border block justify-between px-4 py-3 rounded-sm text-red-600 bg-red-200 border-red-400 max-w-3xl mx-auto space-x-4"
                                role="alert">
                                <div>
                                    {{ $error }}
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            @endif
            <form class="mx-32 my-64" method="POST" action="{{ url('/auth') }}">
                @csrf

                <div class="mb-10 text-slate-600">
                    <h1 class="text-2xl">Selamat datang</h1>
                    <p class="text-sm text-slate-500 font-light">Silakan masukkan email atau nomor telepon dan password Anda
                        untuk mulai
                        menggunakan aplikasi</p>
                </div>

                <div class="sm:col-span-3">
                    <label for="email_or_phone" class="block text-sm font-medium leading-6 text-slate-500">Email / Nomor
                        Telepon</label>
                    <div class="mt-2">
                        <input type="text" name="email_or_phone" id="email_or_phone"
                            placeholder="Contoh: admin@gmail.com"
                            class="block w-full rounded-md border-0 py-1.5 px-3 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    </div>
                </div>
                <div class="sm:col-span-4 mt-4 mb-8">
                    <label for="password" class="block text-sm font-medium leading-6 text-slate-500">Password</label>
                    <div class="mt-2">
                        <input id="password" name="password" type="password" placeholder="Password"
                            class="block w-full rounded-md border-0 py-1.5 px-3 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    </div>
                </div>

                <div class="flex w-full items-center gap-x-6">
                    <button type="submit"
                        class="w-full bg-blue-400 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Masuk</button>
                </div>
            </form>
        </div>
    </div>
@endsection
