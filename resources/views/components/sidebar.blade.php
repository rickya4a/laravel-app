<!-- component -->
@section('sidebar')

<div class="relative flex flex-col bg-white text-gray-700 h-[calc(100vh-2rem)] p-4">
    <nav class="flex flex-col gap-1 min-w-[240px] p-2 font-sans text-base font-normal text-gray-700">
      <div role="button" tabindex="0" class="flex items-center w-full p-3 rounded-lg text-start leading-tight transition-all hover:bg-blue-50 hover:bg-opacity-80 focus:bg-blue-50 focus:bg-opacity-80 active:bg-gray-50 active:bg-opacity-80 hover:text-blue-900 focus:text-blue-900 active:text-blue-900 outline-none">
        <div class="grid place-items-center mr-4">
            <i class="bi bi-house-door"></i>
        </div>
        <a href={{ url('/admin/dashboard') }}>
            Dashboard
        </a>
      </div>
      <div role="button" tabindex="0" class="flex items-center w-full p-3 rounded-lg text-start leading-tight transition-all hover:bg-blue-50 hover:bg-opacity-80 focus:bg-blue-50 focus:bg-opacity-80 active:bg-blue-50 active:bg-opacity-80 hover:text-blue-900 focus:text-blue-900 active:text-blue-900 outline-none">
        <div class="grid place-items-center mr-4">
            <i class="bi bi-person-fill"></i>
        </div>
        <a href={{ url('/admin/users') }}>
            Manajemen User
        </a>
      </div>
      <div role="button" tabindex="0" class="flex items-center w-full p-3 rounded-lg text-start leading-tight transition-all hover:bg-blue-50 hover:bg-opacity-80 focus:bg-blue-50 focus:bg-opacity-80 active:bg-blue-50 active:bg-opacity-80 hover:text-blue-900 focus:text-blue-900 active:text-blue-900 outline-none">
        <div class="grid place-items-center mr-4">
            <i class="bi bi-journal-text"></i>
        </div>
        <a href={{ url('/admin/products') }}>
            Manajemen Product
        </a>
      </div>
    </nav>
  </div>
@endsection
