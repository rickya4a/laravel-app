@extends('components.layout')

@include('components.navbar')

@section('content')
    <div class="container px-10">
        <span class="text-2xl font-semibold text-black">
            <h1>Produk Terbaru</h1>
        </span>
        <div class="carousel carousel-center w-fit p-4 space-x-4">
            @foreach ($latest_product as $row)
                <div class="carousel-item">
                    <div class="card">
                        <img src="https://daisyui.com/images/stock/photo-1559703248-dcaaec9fab78.jpg" class="rounded-box" />
                        <div class="card-body w-3">
                            <h2 class="card-title">{{ $row->name }}</h2>
                            <p>{{ $row->price }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <span class="text-2xl font-semibold text-black">
            <h1>Produk Tersedia</h1>
        </span>

        <div class="grid grid-cols-5 mb-4">
            @foreach ($product as $row)
                <div class="card w-64 mx-4 py-4 border-solid border-1 shadow-xl">
                    <figure><img src="https://daisyui.com/images/stock/photo-1606107557195-0e29a4b5b4aa.jpg"
                            alt="Shoes" />
                    </figure>
                    <div class="card-body">
                        <h2 class="card-title">{{ $row->name }}</h2>
                        <p>{{ $row->price }}</p>
                        <div class="card-actions justify-end">
                            <button class="btn btn-primary">Buy Now</button>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@include('components.footer')
