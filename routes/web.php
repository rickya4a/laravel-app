<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::get('/login', [UserController::class, 'login'])->name('login');
Route::get('/logout', [UserController::class, 'logout'])->name('logout');
Route::post('/auth', [UserController::class, 'auth']);

Route::prefix('admin')->middleware(['auth'])->group(function () {
    Route::get('/dashboard', [UserController::class, 'index']);
    Route::get('/users', [UserController::class, 'users']);
    Route::post('/user', [UserController::class, 'store']);
    Route::get('/products', [ProductController::class, 'products']);
    Route::post('/product', [ProductController::class, 'store']);
    Route::get('/product/delete/{id}', [ProductController::class, 'destroy']);
});
